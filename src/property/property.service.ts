import { Injectable } from '@nestjs/common';
import Property from 'src/entity/property.entity';

@Injectable()
export class PropertyService {
    
    async getAll() {
        const ret  = await Property.createQueryBuilder('property')
            .orderBy('property.createdAt', 'DESC')
            .getMany();

        return ret
    }
    async isNameExisted(name) {
        const prop = await Property.find({
            where: [{ name }],
        });
        return prop.length > 0 ? true : false;
    }
    async searchByKeyword(keyword) {

        const slugify = (str) => {
            str = str.toLowerCase();
            str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
            str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
            str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
            str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
            str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
            str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
            str = str.replace(/(đ)/g, 'd');
            str = str.replace(/([^0-9a-z-\s])/g, '');
            str = str.replace(/(\s+)/g, '-');
            str = str.replace(/^-+/g, '');
            str = str.replace(/-+$/g, '');
            return str;
        };
          
        let slugifiedKey = slugify(keyword)
        const ret = await Property.createQueryBuilder('property')
            .addSelect(`word_similarity('${slugifiedKey}', unaccent(property.reporterName))`, 'namesimi')
            .addSelect(`word_similarity(UPPER('${slugifiedKey}'), property.type::text)`, 'typesimi')
            .where(`word_similarity('${slugifiedKey}', unaccent(property.reporterName)) > 0.6`)
            .orWhere(`word_similarity(UPPER('${slugifiedKey}'), property.type::text) > 0.6`)
            .orderBy('namesimi', 'DESC')
            .addOrderBy('typesimi', 'DESC')
            
            .getMany()
        return ret;
    }

    async createProperty(type, status, rName) {
        const model = Property.create({
            type, furnishStatus: status, reporterName: rName
        })

        return model.save();
    }
}
