import { Column, Entity, ManyToOne } from "typeorm";
import Base from "./base.entity";
import Property from "./property.entity";

@Entity()
export default class Room extends Base { 
    @Column()
    quantity: number;

    @Column()
    name: String;

    @ManyToOne(type => Property, prop => prop.rooms)
    property: Property;
}