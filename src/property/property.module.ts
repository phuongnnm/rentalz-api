import { Module } from '@nestjs/common';
import { PropertyResolver } from 'src/graphql/resolver/property';
import { PropertyService } from './property.service';

@Module({
  providers: [PropertyService, PropertyResolver]
})
export class PropertyModule {}
