import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import Property from "src/entity/property.entity";
import Room from "src/entity/room.entity";
import { PropertyService } from "src/property/property.service";
import { getConnection } from "typeorm";

@Resolver('Property')
export class PropertyResolver {
    constructor(private readonly service: PropertyService) { }

    @Query()
    async getAllProperties() {
        return this.service.getAll();
    }

    @Query()
    async getPropById(@Args('id') id) {
        let ret = await Property.findOne(id, { relations: ['rooms'] })
        return ret
    }

    @Query()
    async searchForPropByKeyWord(@Args('keyword') keyword) {
        return this.service.searchByKeyword(keyword);
    }

    @Mutation()
    async createProperty(@Args('type') type, @Args('furnishStatus') status, @Args('reporterName') rName) {
        return this.service.createProperty(type, status, rName);
    }

    @Mutation()
    async registerProperty(@Args('input') input) {
        return getConnection().transaction(async transactionalEntityManager => {
            const propBasic = ({ rooms, ...property }) => property;

            //Activity 1: Save property's data to Property table
            const createdProp = await transactionalEntityManager.save(
                Property.create({ ...propBasic(input) }),
            );

            const { rooms, ...propWithoutRoom } = input;

            const roomModels = rooms.map((room) => {
                return Room.create({
                    quantity: room.quantity,
                    name: room.name,
                    property: Property.create({ id: createdProp.id })
                })
            });
            //Activity 2: Save rooms' data to Room table
            await transactionalEntityManager.save(roomModels)
            return createdProp
        })
    }

    @Mutation()
    async updateProperty(@Args('id') id, @Args('type') type, @Args('furnishStatus') furnishStatus, @Args('note') note, @Args('rooms') rooms, @Args('price') price) {
        
        return await getConnection().transaction(async transactionalEntityManager => {
            await transactionalEntityManager
                .getRepository(Room)
                .createQueryBuilder('room')
                .leftJoin('room.property', 'property')
                .where("property.id = :id", { id })
                .delete()
                .execute()

                const prop = Property.create({
                    id,
                    type,
                    furnishStatus,
                    note,
                    price
                })
                console.log('==> price');
                console.log(price)
            const roomModels = rooms.map((room) => {
                return Room.create({
                    quantity: room.quantity,
                    name: room.name,
                    property: Property.create({ id })
                })
            })

            await transactionalEntityManager.save(roomModels)
            const ret = await transactionalEntityManager.save(prop)
            return ret
        })
    }

    @Mutation()
    async deleteProperty(@Args('id') id) {
        // const prop = await Property.findOne(id);
        // return Property.remove(prop);

        return await getConnection().transaction(async transactionalEntityManager => {
            await transactionalEntityManager
                .getRepository(Room)
                .createQueryBuilder('room')
                .leftJoin('room.property', 'property')
                .where("property.id = :id", { id })
                .delete()
                .execute()

            const prop = await Property.findOne(id);
            return transactionalEntityManager.remove(prop);
        })
    }
}