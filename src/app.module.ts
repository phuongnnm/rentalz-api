import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { Connection } from 'typeorm';
import { PropertyModule } from './property/property.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      context: ({ req }) => ({ req }),
      typeDefs: /* GraphQL */ `
        scalar Upload
      `,
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
      introspection: true,
    }),
    TypeOrmModule.forRoot(),
    ConfigModule,
    PropertyModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
