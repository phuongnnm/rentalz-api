export enum ProptertyType {
    MANSION = "MANSION",
    BUNGALOW = "BUNGALOW",
    HOUSE = "HOUSE"
}

export enum FurnishStatus {
    FURNISHED = "FURNISHED",
    UNFURNISHED = "UNFURNISHED",
    PARTFURNISHED = "PARTFURNISHED"
}