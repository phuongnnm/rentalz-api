import { FurnishStatus, ProptertyType } from "src/property/enums";
import { Column, Entity, OneToMany } from "typeorm";
import Base from "./base.entity";
import Room from "./room.entity";


@Entity()
export default class Property extends Base { 
    @Column({ type: 'enum', enum: ProptertyType, default: ProptertyType.HOUSE })
    type: ProptertyType;

    @Column({ nullable: true })
    price: number;

    @Column()
    reporterName: String;

    @Column({ nullable: true })
    name: String;

    @Column({ type: 'enum', enum: FurnishStatus, default: FurnishStatus.UNFURNISHED })
    furnishStatus: FurnishStatus;

    @Column({ nullable: true })
    note: String;

    @OneToMany(type => Room, room => room.property)
    rooms: Room[];
}